import java.util.ArrayList;

// 105 536 parts
public class Pizza {
	boolean[][] jambons;

	Part[][] parts;

	int hauteur;
	int largeur;
	int minJambon;
	int maxTaille;

	ArrayList<PartModele> modeles;
	
	static Pizza pizza;

	public static int nbJambonsDansPart(int x, int y, PartModele modele) {
		int nbJambons = 0;
		
		for (int i = x; i < modele.largeur + x; i++) {
			for (int j = y; y < modele.hauteur + y; j++) {
				if (!areCoordsValid(x, y)) {continue;}
				if (pizza.jambons[j][i])
					nbJambons++;
			}
		}
		
		return nbJambons;
	}
	
	public static boolean canBePlaced(int x, int y, PartModele modele) {
		int nbJambons = 0;
		for (int i = x; i < modele.largeur + x; i++) {
			for (int j = y; j < modele.hauteur + y; j++) {
				if (!areCoordsValid(i, j)) {continue;}
				if (pizza.parts[j][i] != null)
					return false;
				if (pizza.jambons[j][i])
					nbJambons++;
			}
		}
		return nbJambons >= pizza.minJambon;
	}
	
	public static void generatePartModeles() {
		pizza.modeles = new ArrayList<PartModele>();
		
		for (int i = 1; i <= pizza.maxTaille; i++)
			for (int j = 1; j <= pizza.maxTaille; j++)
				if (i * j <= 12 && i*j >= pizza.minJambon)
						pizza.modeles.add(new PartModele(i, j));
	}
	
	public static void positionOnePart() {
		int nbFailures = 0;
		while (nbFailures < pizza.modeles.size()) {
			int x = (int) (Math.random() * pizza.largeur);
			int y = (int) (Math.random() * pizza.hauteur);
			
			PartModele modele = pizza.modeles.get(nbFailures);
			if (Pizza.canBePlaced(x, y, modele)) {
				pizza.placePart(x, y, modele);
				nbFailures = 0;
			} else {
				++nbFailures;
			}
		}
	}

	private void placePart(int x, int y, PartModele modele) {
		Part part = new Part();
		part.x = x;
		part.y = y;
		part.modele = modele;
		
		for (int i = x; i < x + modele.largeur; i++ ) {
			for (int j = y; j < y + modele.hauteur; j++) {
				if (areCoordsValid(x, y))
					pizza.parts[y][x] = part;
			}
		}
	}
	
	public static boolean areCoordsValid(int x, int y) {
		return x >= 0 && y >= 0 && x < pizza.largeur && y < pizza.hauteur;
	}
}