import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.PushbackInputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Parser {
private String fileName;
	
	/**
	 * Permet la lecture d'un fichier
	 * @param fileName Nom du fichier
	 */
	public Parser(String fileName) {
		this.fileName = fileName;
	}
	
	
	public Parser read() {
		Pizza pizza = Pizza.pizza = new Pizza();
		try (BufferedReader br = new BufferedReader(new java.io.FileReader(fileName))) {
		    String line;
		    int lineNumber = 0;
		    
		    while ((line = br.readLine()) != null) {
		    	if (lineNumber == 0) {
		    		Scanner scanner = new Scanner(line);
		    		pizza.hauteur = scanner.nextInt();
		    		pizza.largeur = scanner.nextInt();
		    		pizza.minJambon = scanner.nextInt();
		    		pizza.maxTaille = scanner.nextInt();
		    		pizza.jambons = new boolean[pizza.hauteur][pizza.largeur];
		    		pizza.parts = new Part[pizza.hauteur][pizza.largeur];
		    		
		    	} else {
		    		for (int i = 0 ; i < line.length(); ++i) {
		    			pizza.jambons[lineNumber - 1][i] = line.charAt(i) == 'H';
		    		}
		    	}
		    	
		    	++lineNumber;
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public void write(String fname) {
		Set<Part> set = new HashSet<>();
		
		for (Part[] ps : Pizza.pizza.parts) {
			for (Part p : ps) {
				if (p != null)
					set.add(p);
			}
		}
		
		try {
			PrintWriter printWriter = new PrintWriter(fname, "UTF-8");
			
			printWriter.println(set.size());
			
			for (Part p : set) {
				printWriter.println(p.toString());
			}
			
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
