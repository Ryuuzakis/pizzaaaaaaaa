
public class PartModele {
	
	int largeur, hauteur;

	public PartModele(int largeur, int hauteur) {
		this.largeur = largeur;
		this.hauteur = hauteur;
	}
	
	@Override
	public String toString() {
		return "largeur : " + largeur + " hauteur " + hauteur;
	}
	
	public int size() {
		return largeur * hauteur;
	}

}
